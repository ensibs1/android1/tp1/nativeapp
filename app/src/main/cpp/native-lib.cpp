#include <jni.h>
#include <string>
#include <cctype>

extern "C"
JNIEXPORT jstring JNICALL Java_com_andsec_tp1_nativeapp_MainActivity_decrypt(JNIEnv *env, jobject object, jbyteArray array) {
    jbyte *buffer = env->GetByteArrayElements(array, NULL);
    jsize size = env->GetArrayLength(array);

    char res[size];

    char key[] = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
    int lenKey = sizeof(key)/sizeof(char);

    for(int i = 0 ; i < size; i++) {
        res[i] = buffer[i] ^ key[i % lenKey];
    }

    env->ReleaseByteArrayElements(array, buffer, JNI_ABORT);
    return env->NewStringUTF(res);
}