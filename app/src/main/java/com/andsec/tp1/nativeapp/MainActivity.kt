package com.andsec.tp1.nativeapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.andsec.tp1.nativeapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        binding.sampleText.text = decrypt(byteArrayOf(38, 4, 24, 5, 25, 69, 87, 8, 0, 9, 1, 28, 12, 2, 11, 28))
    }

    /**
     * A native method that is implemented by the 'nativeapp' native library,
     * which is packaged with this application.
     */
    external fun decrypt(byteArray: ByteArray): String

    companion object {
        // Used to load the 'nativeapp' library on application startup.
        init {
            System.loadLibrary("nativeapp")
        }
    }
}